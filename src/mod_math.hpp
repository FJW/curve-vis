#ifndef MOD_MATH_HPP
#define MOD_MATH_HPP

#include <cassert>
#include <optional>
#include <ostream>
#include <stdexcept>
#include <string>

#include "typedefs.hpp"

inline u32 mod(u64 n, u32 m) { return static_cast<u32>(n % m); }

inline u32 add_m(u32 a, u32 b, u32 m) { return mod(u64{a} + u64{b}, m); }

inline u32 sub_m(u32 a, u32 b, u32 m) { return mod(u64{m} + u64{a} - u64{b}, m); }

inline u32 mul_m(u32 a, u32 b, u32 m) { return mod(u64{a} * u64{b}, m); }

u32 pow_m(u32 base, u32 exp, u32 m);

inline bool is_square(u32 i, u32 m) { return pow_m(i, (m - 1) / 2u, m) == 1u; }

inline u32 square_m(u32 i, u32 m) { return mul_m(i, i, m); }

u32 inv_m(u32 i, u32 m);

std::optional<u32> sqrt_m(u32 i, u32 m);

class ℤₙ {
public:
	ℤₙ(u32 n, u32 m) : n{n % m}, m{m} { assert(m != 0); }

	ℤₙ operator+() const {
		return *this;
	}

	friend ℤₙ operator+(ℤₙ lhs, ℤₙ rhs) {
		assert(lhs.m == rhs.m);
		return {add_m(lhs.n, rhs.n, lhs.m), lhs.m};
	}

	ℤₙ& operator+=(ℤₙ rhs) {
		*this = *this + rhs;
		return *this;
	}

	friend ℤₙ operator-(ℤₙ lhs, ℤₙ rhs) {
		assert(lhs.m == rhs.m);
		return {sub_m(lhs.n, rhs.n, lhs.m), lhs.m};
	}

	ℤₙ operator-() const { return {m - n, m}; }

	ℤₙ& operator-=(ℤₙ rhs) {
		*this = *this - rhs;
		return *this;
	}

	friend ℤₙ operator*(ℤₙ lhs, ℤₙ rhs) {
		assert(lhs.m == rhs.m);
		return {mul_m(lhs.n, rhs.n, lhs.m), lhs.m};
	}

	ℤₙ& operator*=(ℤₙ rhs) {
		*this = *this * rhs;
		return *this;
	}

	friend ℤₙ operator/(ℤₙ lhs, ℤₙ rhs) {
		assert(lhs.m == rhs.m);
		return {mul_m(lhs.n, inv_m(rhs.n, lhs.m), lhs.m), lhs.m};
	}

	ℤₙ& operator/=(ℤₙ rhs) {
		*this = *this / rhs;
		return *this;
	}

	friend bool operator==(ℤₙ, ℤₙ) = default;

	ℤₙ inv() const { return {inv_m(n, m), m}; }

	ℤₙ pow(u32 i) const { return {pow_m(n, i, m), m}; }

	std::optional<ℤₙ> sqrt() const {
		const auto root = sqrt_m(n, m);
		if (root) {
			return {{*root, m}};
		} else {
			return std::nullopt;
		}
	}

	u32 value() const { return n; }

	u32 modulus() const { return m; }

	std::string to_string() const { return std::to_string(n); }

private:
	u32 n;
	u32 m;
};

inline std::ostream& operator<<(std::ostream& stream, ℤₙ x) { return stream << x.value(); }

#endif
