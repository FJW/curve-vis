#include "primes.hpp"

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdint>
#include <numeric>
#include <utility>
#include <vector>

#include "mod_math.hpp"

namespace {

std::pair<u32, unsigned> prepare_n(u32 n) {
	auto j = 0u;
	while (n % 2u == 0 and n != 0) {
		++j;
		n >>= 1u;
	}
	return {n, j};
}

bool mr_round(u32 a, u32 d, unsigned j, u32 n) {
	auto p = pow_m(a, d, n);
	if (p == 1) {
		return true;
	}
	for (auto i = 0u; i < j; ++i) {
		if (p == n - 1) {
			return true;
		}
		p = square_m(p, n);
	}
	return false;
}

} // anonymous namespace

bool is_prime(u32 n) {
	if (n == 2 or n == 3) {
		return true;
	}
	if (n < 5) {
		return false;
	}
	const auto [d, j] = prepare_n(n - 1);
	if (n < 2047) {
		return mr_round(2, d, j, n);
	}
	for (auto a : {2u, 7u, 61u}) {
		if (not mr_round(a, d, j, n)) {
			return false;
		}
	}
	return true;
}

namespace {
u32 abs_diff(u32 x, u32 y) { return (x > y) ? x - y : y - x; }

std::optional<u32> pollard_rho_round(u32 n, u32 i) {
	const auto f = [n, i](u32 x) { return mod(u64{x} * u64{x} + u64{i}, n); };
	auto x = u32{2};
	auto y = u32{2};
	auto d = u32{1};
	while (d == 1) {
		x = f(x);
		y = f(f(y));
		d = std::gcd(abs_diff(x, y), n);
	}
	if (d == n) {
		return std::nullopt;
	}
	return {d};
}

u32 pollard_rho(u32 n) {
	assert(not is_prime(n));
	assert(n % 2u == 1u);
	for (auto i = u32{1}; i < n; ++i) {
		if (const auto tmp = pollard_rho_round(n, i); tmp) {
			return *tmp;
		}
	}
	throw std::logic_error{"factoring " + std::to_string(n) + " failed"};
}

std::vector<u32> collect_factors(u32 n, std::vector<u32> prime_factors = {}) {
	if (is_prime(n)) {
		prime_factors.push_back(n);
		return prime_factors;
	}
	const auto factor = pollard_rho(n);
	prime_factors = collect_factors(factor, std::move(prime_factors));
	prime_factors = collect_factors(n / factor, std::move(prime_factors));
	return prime_factors;
}
} // anonymous namespace

std::vector<u32> factor(u32 n) {
	if (n < 4) {
		return {n};
	}
	auto ret = std::vector<u32>{};
	while (n % 2 == 0) {
		ret.push_back(2u);
		n /= 2u;
	}
	if (n != 1u) {
		ret = collect_factors(n, std::move(ret));
		std::sort(ret.begin(), ret.end());
	}
	return ret;
}

u32 compute_cofactor(u32 n, const std::vector<u32>& factors) {
	auto ret = u32{1u};
	for (auto factor: factors) {
		if (n % factor == 0) {
			n /= factor;
			ret *= factor;
		}
	}
	return ret;
}
