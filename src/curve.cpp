#include "curve.hpp"

#include <cmath>
#include <exception>
#include <iomanip>
#include <istream>
#include <optional>
#include <ostream>
#include <stdexcept>

#include "mod_math.hpp"
#include "primes.hpp"

point::point(coordinate coord, curve c) : m_x{coord.x}, m_y{coord.y}, c{c} {
	if (not on_curve(coord, c)) {
		throw std::runtime_error{"Point has to be on curve."};
	}
}
point::point(ℤₙ x, ℤₙ y, curve c) : m_x{x}, m_y{y}, c{c} {
	if (x.modulus() != y.modulus() or y.modulus() != c.m) {
		throw std::runtime_error{
		        "The moduli of the coordinates must match that of the curve."};
	}
	if (not on_curve(coordinate{x, y}, c)) {
		throw std::runtime_error{"Point has to be on curve."};
	}
}

point operator+(point l, point r) {
	if (l.c != r.c) {
		throw std::runtime_error{
		        "Points can only be added/subtracted if they are on the same curve."};
	}
	const auto d = ℤₙ{l.c.d, l.c.m};
	const auto one = ℤₙ{1, l.c.m};
	const auto x0 = l.x(), y0 = l.y();
	const auto x1 = r.x(), y1 = r.y();

	const auto prod = d * x0 * y0 * x1 * y1;
	const auto x = (x0 * y1 + y0 * x1) / (one + prod);
	const auto y = (y0 * y1 - x0 * x1) / (one - prod);
	return {x, y, l.c};
}

point operator-(point lhs, point rhs) {
	auto res = lhs + rhs;
	res.m_x = ℤₙ{0, res.c.m} - res.m_x;
	return res;
}

point operator*(point p, u32 scalar) {
	auto ret = point{0u, 1u, p.c};
	auto factor = p;
	while (scalar > 0) {
		const auto b = scalar bitand 1u;
		scalar >>= 1u;
		if (b) {
			ret = ret + factor;
		}
		factor = factor + factor;
	}
	return ret;
}

std::ostream& operator<<(std::ostream& s, const coordinate& p) {
	return s << '(' << std::setw(2) << p.x << "," << std::setw(2) << p.y << ')';
}
std::ostream& operator<<(std::ostream& s, const point& p) {
	return s << '(' << std::setw(2) << p.x() << "," << std::setw(2) << p.y() << ')';
}

std::ostream& operator<<(std::ostream& s, const raw_coordinate& p) {
	return s << '(' << std::setw(2) << p.x << "," << std::setw(2) << p.y << ')';
}
std::istream& operator>>(std::istream& s, raw_coordinate& p) {
	auto c = char{};
	s >> p.x >> c >> p.y;
	if (c != ',') {
		s.setstate(std::ios_base::failbit);
	}
	return s;
}

coordinate operator%(raw_coordinate c, u32 m) { return coordinate{ℤₙ{c.x, m}, ℤₙ{c.y, m}}; }

bool on_curve(coordinate p, curve c) {
	assert(p.x.modulus() == p.y.modulus());
	const auto one = ℤₙ{1, c.m};
	const auto d = ℤₙ{c.d, c.m};
	const auto x = p.x, y = p.y;
	return x * x + y * y == one + d * x * x * y * y;
}

bool is_curve_generator(point p) {
	const auto order = point_order(p);
	const auto mod = p.get_curve().m;
	const auto rhs = 2 * std::sqrt(mod);
	const auto lhs = order > (mod + 1) ? order - (mod + 1) : mod + 1 - order;
	return lhs <= rhs;
}

std::optional<ℤₙ> get_y(curve c, ℤₙ x) {
	const auto one = ℤₙ{1, c.m};
	const auto d = ℤₙ{c.d, c.m};
	const auto x_squared = x * x;
	const auto y_squared = (x_squared - one) / ((d * x_squared) - one);
	return y_squared.sqrt();
}

point find_point(curve c) {
	const auto one = ℤₙ{1, c.m};
	auto x = ℤₙ{2, c.m};
	while (true) { // there is always some point
		const auto y = get_y(c, x);
		if (y != std::nullopt and on_curve({x, *y}, c)) {
			return {x, *y, c};
		}
		x += one;
	}
}

point find_curve_generator(curve c) {
	const auto one = ℤₙ{1, c.m};
	auto x = ℤₙ{2, c.m};
	while (true) { // there is always some point
		const auto y = get_y(c, x);
		if (y != std::nullopt and on_curve({x, *y}, c)) {
			const auto p = point{{x, *y}, c};
			if (is_curve_generator(p)) {
				return p;
			} else if (const auto q = point{p.x(), -p.y(), p.get_curve()};
			           is_curve_generator(q)) {
				return q;
			}
		}
		x += one;
	}
}

std::vector<point> collect_points(curve c) {
	const auto zero = ℤₙ{0, c.m};
	auto ret = std::vector<point>{};
	for (auto x_ = u32{}; x_ < c.m; ++x_) {
		const auto x = ℤₙ{x_, c.m};
		const auto y = get_y(c, x);
		if (y == std::nullopt) {
			continue;
		} else if (y->value() == 0) {
			ret.push_back({x, zero, c});
		} else {
			ret.push_back({x, *y, c});
			ret.push_back({x, -*y, c});
		}
	}
	return ret;
}

std::vector<std::pair<point, u32>> collect_points_with_order(curve c) {
	const auto neutral_element = point{{{0, c.m}, {1, c.m}}, c};
	const auto g = find_curve_generator(c);
	const auto order = point_order(g);
	const auto factors = factor(order);
	auto ret = std::vector{std::pair{neutral_element, u32{1}}, std::pair{g, order}};
	auto p = g + g;
	for (auto i = u32{2}; i < order; ++i) {
		const auto point_order = order / compute_cofactor(i, factors);
		ret.emplace_back(p, point_order);
		p = p + g;
	}
	return ret;
}

u32 count_points(curve c) {
	const auto zero = ℤₙ{0, c.m};
	auto ret = u32{};
	for (auto x_ = u32{}; x_ < c.m; ++x_) {
		const auto x = ℤₙ{x_, c.m};
		const auto y = get_y(c, x);
		if (y == std::nullopt) {
			continue;
		} else if (*y == zero) {
			++ret;
		} else {
			ret += 2u;
		}
	}
	return ret;
}

u32 point_order(point p) {
	assert(on_curve(coordinate{p.x(), p.y()}, p.get_curve()));
	auto ret = u32{};
	auto q = p;
	do {
		assert(on_curve(coordinate{q.x(), q.y()}, q.get_curve()));
		q = p + q;
		++ret;
	} while (p != q);
	return ret;
}

