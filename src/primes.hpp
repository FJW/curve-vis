#ifndef PRIMES_HPP
#define PRIMES_HPP

#include <vector>

#include "typedefs.hpp"

bool is_prime(u32 n);

std::vector<u32> factor(u32 n);

u32 compute_cofactor(u32 n, const std::vector<u32>& factors);

#endif
