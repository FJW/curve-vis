#ifndef CURVE_HPP
#define CURVE_HPP

#include <optional>
#include <ostream>
#include <stdexcept>
#include <vector>

#include "mod_math.hpp"
#include "typedefs.hpp"

struct curve {
	u32 d;
	u32 m;

	friend bool operator==(const curve&, const curve&) = default;
};

struct coordinate {
	ℤₙ x;
	ℤₙ y;

	friend bool operator==(const coordinate&, const coordinate&) = default;
};
std::ostream& operator<<(std::ostream& s, const coordinate& p);


struct raw_coordinate {
	u32 x;
	u32 y;

	friend bool operator==(const raw_coordinate&, const raw_coordinate&) = default;
};
std::ostream& operator<<(std::ostream& s, const raw_coordinate& p);
std::istream& operator>>(std::istream& s, raw_coordinate& p);
coordinate operator%(raw_coordinate c, u32 m);

class point {
public:
	point(coordinate coord, curve c);
	point(ℤₙ x, ℤₙ y, curve c);
	point(u32 x, u32 y, curve c) : point{{x, c.m}, {y, c.m}, c} {}

	friend bool operator==(const point&, const point&) = default;

	friend point operator+(point lhs, point rhs);
	friend point operator-(point lhs, point rhs);
	friend point operator*(point p, u32 scalar);
	friend point operator*(u32 scalar, point p) { return p * scalar; }

	ℤₙ x() const { return m_x; }
	ℤₙ y() const { return m_y; }
	curve get_curve() const { return c; }

private:
	ℤₙ m_x;
	ℤₙ m_y;
	curve c;
};
std::ostream& operator<<(std::ostream& s, const point& p);

bool on_curve(coordinate coord, curve c);

std::optional<ℤₙ> get_y(curve c, ℤₙ x);

point find_point(curve c);

std::vector<point> collect_points(curve c);

std::vector<std::pair<point, u32>> collect_points_with_order(curve c);

u32 count_points(curve c);

u32 point_order(point p);

bool is_curve_generator(point p);

point find_curve_generator(curve c);
#endif
