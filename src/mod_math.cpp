#include "mod_math.hpp"

#include "primes.hpp"

#include <array>
#include <cassert>
#include <optional>
#include <stdexcept>
#include <string>
#include <tuple>
#include <valarray>

u32 pow_m(u32 base, u32 exp, u32 m) {
	auto ret = u32{1u};
	auto factor = base;
	while (exp > 0) {
		const auto b = exp bitand 1u;
		exp >>= 1u;
		if (b) {
			ret = mul_m(ret, factor, m);
		}
		factor = square_m(factor, m);
	}
	return ret;
}

namespace {
std::array<i64, 3> xgcd(u32 a_, u32 b_) {
	auto a = std::array<i64, 3>{a_, 1, 0};
	auto b = std::array<i64, 3>{b_, 0, 1};
	while (b[0] != 0) {
		const auto r = a[0] / b[0];
		a[0] -= r * b[0];
		a[1] -= r * b[1];
		a[2] -= r * b[2];
		std::swap(a, b);
	}
	return a;
}
} // anonymous namespace

u32 inv_m(u32 i, u32 m) {
	assert(i != 0);
	const auto [gcd, i_inv, m_inv] = xgcd(i, m);
	assert(gcd == 1);
	assert(i_inv < m);
	if (gcd != 1) {
		throw std::runtime_error{std::to_string(i) + " and " + std::to_string(m) +
		                         " are not coprime (gcd = " + std::to_string(gcd) + ")"};
	}
	if (i_inv < 0) {
		return static_cast<u32>(i_inv + m);
	} else {
		return static_cast<u32>(i_inv);
	}
}

namespace {
std::tuple<u32, u32> remove_zeros(u32 n) {
	assert(n != 0);
	auto q = n;
	auto s = u32{};
	while (q % 2u == 0u and q != 0) {
		++s;
		q /= 2u;
	}
	return {q, s};
}

u32 get_non_square(u32 p) {
	assert(is_prime(p));
	auto x = u32{2};
	while (is_square(x, p)) {
		++x;
	}
	return x;
}

u32 get_order(u32 t, u32 p) {
	auto i = u32{0};
	while (t != 1) {
		t = square_m(t, p);
		++i;
	}
	return i;
}

// according to English wikipedia
u32 tonelli_shanks(u32 n, u32 p) {
	assert(is_prime(p));
	assert(pow_m(n, (p - 1) / 2, p) == 1);
	const auto [q, s] = remove_zeros(p - 1u);
	const auto z = get_non_square(p);
	auto m = s;
	auto c = pow_m(z, q, p);
	auto t = pow_m(n, q, p);
	auto r = pow_m(n, (q + 1u) / 2u, p);
	while (t > 1) {
		const auto i = get_order(t, p);
		const auto b = pow_m(c, u32{1} << (m - i - 1), p);
		m = i;
		c = square_m(b, p);
		t = mul_m(t, square_m(b, p), p);
		r = mul_m(r, b, p);
	}
	if (t == 0) {
		return 0;
	}
	if (r > p / 2u) {
		return p - r;
	}
	return r;
}
} // anonymous namespace

std::optional<u32> sqrt_m(u32 i, u32 m) {
	if (i == 0) {
		return {0};
	}
	if (pow_m(i, (m - 1u) / 2u, m) != 1u) {
		return std::nullopt;
	}
	if (m % 4u != 3u) {
		return tonelli_shanks(i, m);
	}
	auto candidat = pow_m(i, (m + 1u) / 4u, m);
	if (candidat > m / 2u) {
		return m - candidat;
	}
	return candidat;
}
